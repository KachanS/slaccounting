<?php
namespace Sl\Accounting\Handler\Manager;

use Sl\Accounting\HandlerInterface;
use Sl\Accounting\Handler\ManagerInterface;

class Simple implements ManagerInterface
{
    protected $handlers = array();
    protected $defaultHandler = null;

    public function get($name)
    {
        if(isset($this->handlers[$name])) {
            return $this->handlers[$name];
        }
        return $this->defaultHandler;
    }

    public function register(HandlerInterface $handler, $name)
    {
        $this->handlers[$name] = $handler;
        return $this;
    }

    public function setDefaultHandler(HandlerInterface $handler)
    {
        $this->defaultHandler = $handler;
        return $this;
    }
}
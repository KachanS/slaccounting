<?php
namespace Sl\Accounting\Handler;

use Sl\Accounting\HandlerInterface;

interface ManagerInterface
{
    /**
     *
     * @param HandlerInterface $handler
     * @param mixed $name
     */
    public function register(HandlerInterface $handler, $name);

    /**
     *
     * @param mixed $name
     * @return HandlerInterface
     */
    public function get($name);
}
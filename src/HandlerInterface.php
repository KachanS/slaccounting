<?php
namespace Sl\Accounting;

use Sl\Accounting\Model\DocumentInterface;

interface HandlerInterface
{
    public function createTransactions(DocumentInterface $document);
    public function findCashflowAccount(DocumentInterface $document);
    public function findCreditAccount(DocumentInterface $document);
    public function findDebitAccount(DocumentInterface $document);
}
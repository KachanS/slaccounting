<?php
namespace Sl\Accounting;

interface AccountingInterface
{
    public function getName();
    
    public function getTransaction();
    public function getAccount();
}
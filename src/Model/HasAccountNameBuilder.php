<?php
namespace Sl\Accounting\Model;

use Sl\Accounting\Model\AccountInterface;

trait HasAccountNameBuilder
{
    public function buildName($code = null, $title = null)
    {
        if(!($this instanceof AccountInterface)) {
            throw new \Exception('Class must implements AccountInterface to use this method. '.__METHOD__);
        }
        if(is_null($code)) {
            $code = $this->getCode();
        }
        if(is_null($title)) {
            $title = $this->getTitle();
        }
        return implode(' ', array_filter(array($code, $title)));
    }
}
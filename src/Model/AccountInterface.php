<?php
namespace Sl\Accounting\Model;

use Sl\Model\TreeModelInterface;

interface AccountInterface extends TreeModelInterface
{
    public function setTitle($title);
    public function setCode($code);

    public function getTitle();
    public function getCode();

    public function buildName();
}
<?php
namespace Sl\Accounting\Model;

use Sl\Model\ModelInterface;
use Sl\Model\NameableInterface;

interface TransactionInterface extends ModelInterface, NameableInterface
{
    public function setDate($date);
    public function setAmount($amount);

    public function getDate();
    public function getAmount();
}
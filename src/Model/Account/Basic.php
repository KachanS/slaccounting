<?php
namespace Sl\Accounting\Model\Account;

use Sl\Accounting\Model\AccountInterface;
use Sl\Accounting\Model\HasAccountNameBuilder;

abstract class Basic extends \Sl\Model\Tree implements AccountInterface
{
    use HasAccountNameBuilder;

    protected $title;
    protected $code;
    
    public function getCode()
    {
        return $this->code;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
}
<?php
namespace Sl\Accounting\Manager;

use Sl\Accounting\AccountingInterface;
use Sl\Accounting\Exception;

class Basic implements ManagerInterface
{
    protected $accountings = array();

    /**
     *
     * @return AccountingInterface[]
     */
    public function getRegistered()
    {
        return $this->accountings;
    }

    /**
     *
     * @param AccountingInterface $accounting
     * @return \Sl\Accounting\Manager\Basic
     * @throws Exception\Manager
     */
    public function register(AccountingInterface $accounting)
    {
        if(isset($this->accountings[$accounting->getName()])) {
            throw new Exception\Manager('Accounting named "'.$accounting->getName().'" already registered');
        }
        $this->accountings[$accounting->getName()] = $accounting;
        return $this;
    }

    /**
     *
     * @param string $name
     * @return AccountingInterface
     */
    public function find($name)
    {
        return isset($this->accountings[$name])?$this->accountings[$name]:null;
    }
}
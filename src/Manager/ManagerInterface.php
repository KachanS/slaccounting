<?php
namespace Sl\Accounting\Manager;

use Sl\Accounting\AccountingInterface;

interface ManagerInterface
{
    public function register(AccountingInterface $accounting);
    public function find($name);
    public function getRegistered();
}
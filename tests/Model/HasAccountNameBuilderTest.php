<?php
namespace Sl\Accounting\Model;

class HasAccountNameBuilderTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Basic
     */
    protected $nameBuilder;

    public function setUp()
    {
        $this->nameBuilder = $this->getMockForAbstractClass(Account\Basic::class);
    }

    public function testEmpty()
    {
        $mock = $this->getMockForTrait(HasAccountNameBuilder::class);

        $this->setExpectedException(\Exception::class, 'Accounting');

        $mock->buildName();
    }

    public function testBuildName()
    {
        $this->assertEquals('', $this->nameBuilder->buildName());

        $this->assertStringStartsWith('someCode', $this->nameBuilder->buildName('someCode'));
        $this->assertStringStartsWith('someCode', $this->nameBuilder->buildName('someCode', 'someTitle'));

        $this->assertStringEndsWith('someTitle', $this->nameBuilder->buildName(null, 'someTitle'));
        $this->assertStringEndsWith('someTitle', $this->nameBuilder->buildName('someCode', 'someTitle'));

        $this->assertEquals('someCode someTitle', $this->nameBuilder->buildName('someCode', 'someTitle'));

        $this->nameBuilder->setCode('someCode');
        $this->assertStringStartsWith('someCode', $this->nameBuilder->buildName());
        $this->assertStringStartsWith('otherCode', $this->nameBuilder->buildName('otherCode'));

        $this->nameBuilder->setTitle('someTitle');
        $this->assertStringEndsWith('someTitle', $this->nameBuilder->buildName());
        $this->assertStringEndsWith('otherTitle', $this->nameBuilder->buildName(null, 'otherTitle'));
    }
}
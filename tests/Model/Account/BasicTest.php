<?php
namespace Sl\Accounting\Model\Account;

class BasicTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Basic
     */
    protected $account;

    public function setUp()
    {
        $this->account = $this->getMockForAbstractClass(Basic::class);
    }

    /**
     * @dataProvider getSetDataProvider
     */
    public function testSetGet($property, $value)
    {
        $setter = \Sl\Helper\Model::buildSetter($property);
        $getter = \Sl\Helper\Model::buildGetter($property);

        $this->account->$setter($value);

        $this->assertEquals($value, $this->account->$getter());
    }

    public function getSetDataProvider()
    {
        return array(
            array('code', (string) rand(0, 10)),
            array('title', (string) rand(0, 10)),
        );
    }
}
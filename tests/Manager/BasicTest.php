<?php
namespace Sl\Accounting\Manager;

use Sl\Accounting\AccountingInterface;
use Sl\Accounting\Exception;

class BasicTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Basic
     */
    protected $manager;

    /**
     *
     * @var AccountingInterface
     */
    protected $accounting;

    public function setUp()
    {
        $this->manager = new Basic();

        $this->accounting = $this->getMockForAbstractClass(AccountingInterface::class);
        $this->accounting   ->expects($this->any())
                            ->method('getName')
                            ->will($this->returnValue('default'));
    }

    public function testInitializedEmpty()
    {
        $this->assertAttributeEmpty('accountings', $this->manager);
    }

    public function testRegister()
    {
        $this->manager->register($this->accounting);
        $this->assertAttributeNotEmpty('accountings', $this->manager);
        $this->assertAttributeContainsOnly(AccountingInterface::class, 'accountings', $this->manager);

        $this->setExpectedException(Exception\Manager::class);
        $this->manager->register($this->accounting);
    }

    public function testFind()
    {
        $this->assertNull($this->manager->find('default'));
        $this->manager->register($this->accounting);
        $this->assertEquals($this->accounting, $this->manager->find('default'));
    }

    public function testGetRegistered()
    {
        $this->assertEmpty($this->manager->getRegistered());
        $this->manager->register($this->accounting);
        $this->assertCount(1, $this->manager->getRegistered());
        $this->assertArrayHasKey($this->accounting->getName(), $this->manager->getRegistered());
        $this->assertEquals($this->accounting, $this->manager->getRegistered()[$this->accounting->getName()]);
    }
}
<?php
namespace Sl\Accounting\Handler\Manager;

use Sl\Accounting\HandlerInterface;

class SimpleTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Simple
     */
    protected $manager;

    /**
     *
     * @var HandlerInterface
     */
    protected $handler;
    
    public function setUp()
    {
        $this->manager = new Simple();

        $this->handler = $this->getMock(HandlerInterface::class);
    }

    public function testRegister()
    {
        $this->assertAttributeEmpty('handlers', $this->manager);

        $this->manager->register($this->handler, 'someName');
        $this->assertAttributeContains($this->handler, 'handlers', $this->manager);
        $this->assertAttributeContainsOnly(HandlerInterface::class, 'handlers', $this->manager);
        $this->assertAttributeCount(1, 'handlers', $this->manager);

        $this->manager->register($this->handler, 'newName');
        $this->assertAttributeContainsOnly(HandlerInterface::class, 'handlers', $this->manager);
        $this->assertAttributeCount(2, 'handlers', $this->manager);

        $this->manager->register($this->handler, 'someName');
        $this->assertAttributeCount(2, 'handlers', $this->manager);
    }

    public function testGet()
    {
        $this->assertNull($this->manager->get('someName'));

        $this->manager->register($this->handler, 'newName');
        $this->assertNull($this->manager->get('someName'));
        $this->assertInstanceOf(HandlerInterface::class, $this->manager->get('newName'));
    }

    public function testSetDefault()
    {
        $this->assertNull($this->manager->get('someName'));

        $this->manager->setDefaultHandler($this->handler);
        $this->assertSame($this->handler, $this->manager->get('someName'));
    }
}